msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-04 01:37+0000\n"
"PO-Revision-Date: 2024-01-21 14:12\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/skanpage/skanpage.pot\n"
"X-Crowdin-File-ID: 48798\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "tds00@qq.com"

#: DocumentModel.cpp:52 DocumentModel.cpp:69 DocumentModel.cpp:80
#, kde-format
msgid "New document"
msgstr "新文档"

#: DocumentModel.cpp:72
#, kde-format
msgctxt "for file names, indicates a range: from file0000.png to file0014.png"
msgid "%1 ... %2"
msgstr "%1 ... %2"

#: DocumentPrinter.cpp:33
#, kde-format
msgid "Nothing to print."
msgstr "无可打印内容。"

#: DocumentPrinter.cpp:37
#, kde-format
msgid "Previous printing operation still in progress."
msgstr "之前的打印作业仍在进行中。"

#: DocumentPrinter.cpp:69
#, kde-format
msgid "Document sent to printer."
msgstr "文档已发送到打印机。"

#: DocumentSaver.cpp:34
#, kde-format
msgid "No file path given."
msgstr "没有指定文件路径。"

#: DocumentSaver.cpp:38
#, kde-format
msgid "Nothing to save."
msgstr "没有可供保存的图像。"

#: DocumentSaver.cpp:42
#, kde-format
msgid "Saving to non-local directories is currently unsupported."
msgstr "本程序目前不支持保存到非本地目录。"

#: DocumentSaver.cpp:67 DocumentSaver.cpp:98
#, kde-format
msgctxt "%1 is the error message"
msgid "An error ocurred while saving: %1."
msgstr "保存 %1 时发生错误。"

#: DocumentSaver.cpp:79
#, kde-format
msgid "Document saved as PDF."
msgstr "文档已保存为 PDF。"

#: DocumentSaver.cpp:111
#, kde-format
msgid "Document saved with OCR as PDF."
msgstr "文档已通过 OCR 保存为 PDF。"

#: DocumentSaver.cpp:183
#, kde-format
msgid "Document saved as image."
msgstr "文档已保存为图像。"

#: DocumentSaver.cpp:192
#, kde-format
msgid "Failed to save document as image."
msgstr "无法将文档保存为图像。"

#: DocumentSaver.cpp:207
#, kde-format
msgid "Failed to save image"
msgstr "无法保存图像"

#: main.cpp:56
#, kde-format
msgid "Skanpage"
msgstr "Skanpage"

#: main.cpp:58
#, kde-format
msgid "Multi-page scanning application by KDE."
msgstr "KDE 多页扫描应用程序"

#: main.cpp:60
#, kde-format
msgid "© 2015-2022 Kåre Särs, Alexander Stippich"
msgstr "© 2015-2022 Kåre Särs, Alexander Stippich"

#: main.cpp:63 main.cpp:64
#, kde-format
msgid "developer"
msgstr "开发人员"

#: main.cpp:75
#, kde-format
msgid "Sane scanner device name. Use 'test' for test device."
msgstr "Sane 扫描仪设备名称。测试设备请使用“test”。"

#: main.cpp:75
#, kde-format
msgid "device"
msgstr "设备"

#: qml/ContentView.qml:18
#, kde-format
msgctxt "Prefix for document name indicating an unsaved document"
msgid "* %1"
msgstr "* %1"

#: qml/DeviceSelection.qml:54
#, kde-format
msgctxt ""
"Device vendor with device model, followed by the device name identifier"
msgid ""
"%1 %2\n"
"(%3)"
msgstr ""
"%1 %2\n"
"(%3)"

#: qml/DeviceSelection.qml:69
#, kde-kuit-format
msgctxt "@info"
msgid "No devices found."
msgstr "没有找到设备。"

#: qml/DeviceSelection.qml:86
#, kde-format
msgid "Open selected device"
msgstr "打开所选设备"

#: qml/DeviceSelection.qml:95
#, kde-format
msgid "Reload devices list"
msgstr "刷新设备列表"

#: qml/DevicesLoading.qml:29
#, kde-kuit-format
msgctxt "@info"
msgid "Searching for available devices."
msgstr "正在扫描可用设备。"

#: qml/DocumentList.qml:74
#, kde-format
msgid "Select previous page"
msgstr "选择上一页"

#: qml/DocumentList.qml:80
#, kde-format
msgid "Select next page"
msgstr "选择下一页"

#: qml/DocumentList.qml:86
#, kde-format
msgid "Move selected page up"
msgstr "上移所选页面"

#: qml/DocumentList.qml:95
#, kde-format
msgid "Move selected page down"
msgstr "下移所选页面"

#: qml/DocumentList.qml:207
#, kde-kuit-format
msgctxt "@info"
msgid "Processing page..."
msgstr "正在处理页面..."

#: qml/DocumentList.qml:220
#, kde-format
msgctxt "Page index"
msgid "Page %1"
msgstr "第 %1 页"

#: qml/DocumentList.qml:231
#, kde-format
msgid "Move Up"
msgstr "上移"

#: qml/DocumentList.qml:241
#, kde-format
msgid "Move Down"
msgstr "下移"

#: qml/DocumentList.qml:251 qml/DocumentPage.qml:418
#, kde-format
msgid "Rotate Left"
msgstr "旋转 (向左)"

#: qml/DocumentList.qml:257 qml/DocumentPage.qml:425
#, kde-format
msgid "Rotate Right"
msgstr "旋转 (向右)"

#: qml/DocumentList.qml:263 qml/DocumentPage.qml:432
#, kde-format
msgid "Flip"
msgstr "翻转"

#: qml/DocumentList.qml:269 qml/DocumentPage.qml:439
#, kde-format
msgid "Save Page"
msgstr "保存页面"

#: qml/DocumentList.qml:275 qml/DocumentPage.qml:446
#, kde-format
msgid "Delete Page"
msgstr "删除页面"

#: qml/DocumentList.qml:294
#, kde-format
msgid "%1 page"
msgid_plural "%1 pages"
msgstr[0] "%1 页"

#: qml/DocumentList.qml:303
#, kde-format
msgid "Reorder Pages"
msgstr "页面重新排序"

#: qml/DocumentList.qml:308
#, kde-format
msgctxt "Indicates how pages are going to be reordered"
msgid "13 24 → 1234"
msgstr "13 24 → 1234"

#: qml/DocumentList.qml:315
#, kde-format
msgctxt "Indicates how pages are going to be reordered"
msgid "13 42 → 1234"
msgstr "13 42 → 1234"

#: qml/DocumentList.qml:322
#, kde-format
msgid "Reverse Order"
msgstr "反向排序"

#: qml/DocumentPage.qml:56
#, kde-format
msgid "Show Preview"
msgstr "显示预览"

#: qml/DocumentPage.qml:72
#, kde-kuit-format
msgctxt "@info"
msgid "You do not have any images in this document.<nl/><nl/>Start scanning!"
msgstr "此文档尚未包含任何图像。<nl/><nl/>请开始扫描。"

#: qml/DocumentPage.qml:258
#, kde-format
msgid "Discard this selection"
msgstr "丢弃此选区"

#: qml/DocumentPage.qml:278
#, kde-format
msgid "Add Selection Area"
msgstr "添加选区"

#: qml/DocumentPage.qml:283
#, kde-format
msgid "Click and drag to select another area"
msgstr "点击并拖动以选择另一个区域"

#: qml/DocumentPage.qml:293
#, kde-format
msgid "Split Scan Vertically"
msgstr "垂直拆分扫描"

#: qml/DocumentPage.qml:304
#, kde-format
msgid "Split Scan Horizontally"
msgstr "水平拆分扫描"

#: qml/DocumentPage.qml:376
#, kde-format
msgid "Zoom In"
msgstr "放大"

#: qml/DocumentPage.qml:385
#, kde-format
msgid "Zoom Out"
msgstr "缩小"

#: qml/DocumentPage.qml:394
#, kde-format
msgid "Zoom Fit"
msgstr "自适应缩放"

#: qml/DocumentPage.qml:410
#, kde-format
msgid "Zoom 100%"
msgstr "缩放至 100%"

#: qml/ExportWindow.qml:21 qml/MainWindow.qml:103
#, kde-format
msgid "Export PDF"
msgstr "导出 PDF"

#: qml/ExportWindow.qml:44
#, kde-format
msgid "Title:"
msgstr "标题："

#: qml/ExportWindow.qml:60
#, kde-format
msgid "File:"
msgstr "文件："

#: qml/ExportWindow.qml:85
#, kde-format
msgid "Enable optical character recognition (OCR)"
msgstr "启用光学字符识别 (OCR)"

#: qml/ExportWindow.qml:105
#, kde-format
msgid "Languages:"
msgstr "语言："

#: qml/ExportWindow.qml:135
#, kde-format
msgid "%1 [%2]"
msgstr "%1 [%2]"

#: qml/ExportWindow.qml:156
#, kde-format
msgid ""
"If your required language is not listed, please install Tesseract's language "
"file with your package manager."
msgstr ""
"如果您所需的语言未被列出，请使用本机系统的包管理器安装 Tesseract 的语言文件。"

#: qml/ExportWindow.qml:182
#, kde-format
msgid "Save"
msgstr "保存"

#: qml/ExportWindow.qml:198 qml/MainWindow.qml:136
#, kde-format
msgid "Cancel"
msgstr "取消"

#: qml/GlobalMenu.qml:26
#, kde-format
msgctxt "menu category"
msgid "File"
msgstr "文件"

#: qml/GlobalMenu.qml:68
#, kde-format
msgctxt "menu category"
msgid "Scan"
msgstr "扫描"

#: qml/GlobalMenu.qml:124
#, kde-format
msgctxt "menu category"
msgid "Help"
msgstr "帮助"

#: qml/InProgressPage.qml:26
#, kde-kuit-format
msgctxt "Countdown string with time given in seconds"
msgid "Next scan starts in<nl/>%1 s"
msgstr "下次扫描将在<nl/>%1 秒后开始"

#: qml/InProgressPage.qml:44
#, kde-format
msgctxt "@info"
msgid "Scan in progress."
msgstr "正在扫描。"

#: qml/MainWindow.qml:23
#, kde-format
msgctxt "document title: app title"
msgid "%1 ― Skanpage"
msgstr "%1 ― Skanpage"

#: qml/MainWindow.qml:85
#, kde-format
msgid "Discard All"
msgstr "全部丢弃"

#: qml/MainWindow.qml:94
#, kde-format
msgid "Save All"
msgstr "全部保存"

#: qml/MainWindow.qml:111
#, kde-format
msgid "Preview"
msgstr "预览"

#: qml/MainWindow.qml:123
#, kde-format
msgid "Scan"
msgstr "扫描"

#: qml/MainWindow.qml:145
#, kde-format
msgid "Show Scanner Options"
msgstr "显示扫描仪选项"

#: qml/MainWindow.qml:155 qml/ShareWindow.qml:20
#, kde-format
msgid "Share"
msgstr "分享"

#: qml/MainWindow.qml:163
#, kde-format
msgid "Print"
msgstr "打印"

#: qml/MainWindow.qml:184 qml/MainWindow.qml:381
#, kde-format
msgid "About Skanpage"
msgstr "关于 Skanpage"

#: qml/MainWindow.qml:191
#, kde-format
msgid "Configure Skanpage…"
msgstr "配置 Skanpage…"

#: qml/MainWindow.qml:199
#, kde-format
msgid "Configure Keyboard Shortcuts…"
msgstr "配置键盘快捷键…"

#: qml/MainWindow.qml:207
#, kde-format
msgid "Quit"
msgstr "退出"

#: qml/OptionDelegate.qml:55
#, kde-format
msgid "%1:"
msgstr "%1："

#: qml/OptionDelegate.qml:150
#, kde-format
msgctxt "Add ':' to make a header text"
msgid "%1:"
msgstr "%1："

#: qml/OptionDelegate.qml:154
#, kde-format
msgctxt "@option:check a noun, as in 'the default setting'"
msgid "Default"
msgstr "默认"

#: qml/OptionDelegate.qml:168
#, kde-format
msgctxt "%1 is a numeric value"
msgid "Brightness: %1"
msgstr "亮度：%1"

#: qml/OptionDelegate.qml:181
#, kde-format
msgctxt "%1 is a numeric value"
msgid "Contrast: %1"
msgstr "对比度：%1"

#: qml/OptionDelegate.qml:194
#, kde-format
msgctxt "%1 is a numeric value"
msgid "Gamma: %1"
msgstr "伽玛值：%1"

#: qml/OptionDelegate.qml:224
#, kde-format
msgctxt "Adding unit suffix"
msgid "%1 %2"
msgstr "%1 %2"

#: qml/OptionDelegate.qml:249
#, kde-format
msgctxt "Unit suffix for bit"
msgid "bit"
msgstr "位"

#: qml/OptionDelegate.qml:252
#, kde-format
msgctxt "Unit suffix for DPI"
msgid "DPI"
msgstr "DPI"

#: qml/OptionDelegate.qml:255
#, kde-format
msgctxt "Unit suffix for microsecond"
msgid "µs"
msgstr "微秒"

#: qml/OptionDelegate.qml:258
#, kde-format
msgctxt "Unit suffix for second"
msgid "s"
msgstr "秒"

#: qml/OptionDelegate.qml:261
#, kde-format
msgctxt "Unit suffix for millimeter"
msgid "mm"
msgstr "毫米"

#: qml/OptionDelegate.qml:264
#, kde-format
msgctxt "Unit suffix for percent"
msgid "%"
msgstr "%"

#: qml/OptionDelegate.qml:267
#, kde-format
msgctxt "Unit suffix for pixel"
msgid "px"
msgstr "像素"

#: qml/OptionsPanel.qml:30
#, kde-format
msgid "Select options for quick access:"
msgstr "选择快速访问选项："

#: qml/OptionsPanel.qml:70
#, kde-format
msgctxt "scanner device vendor and model"
msgid "%1 %2"
msgstr "%1 %2"

#: qml/OptionsPanel.qml:83
#, kde-format
msgid "Show More"
msgstr "显示更多"

#: qml/OptionsPanel.qml:93
#, kde-format
msgid "Configure Visibility"
msgstr "配置可见性"

#: qml/OptionsPanel.qml:108
#, kde-format
msgid "Reselect Scanner"
msgstr "重新选择扫描仪"

#: qml/SettingsWindow.qml:20
#, kde-format
msgid "Configure"
msgstr "配置"

#: qml/SettingsWindow.qml:38
#, kde-format
msgid "Devices to show:"
msgstr "显示的设备："

#: qml/SettingsWindow.qml:39
#, kde-format
msgctxt "@option:radio Devices to show for scanning"
msgid "Scanners only"
msgstr "仅扫描仪"

#: qml/SettingsWindow.qml:46
#, kde-format
msgctxt "@option:radio Devices to show for scanning"
msgid "Scanners, cameras, and virtual devices"
msgstr "扫描仪、数码相机、虚拟设备"

#: qml/SettingsWindow.qml:53
#, kde-format
msgid "Default file format:"
msgstr "默认文件格式："

#: qml/SettingsWindow.qml:68
#, kde-format
msgid "Default save location:"
msgstr "默认保存位置："

#: qml/SettingsWindow.qml:101
#, kde-format
msgid "Close"
msgstr "关闭"

#: qml/ShareWindow.qml:42
#, kde-format
msgid "Share as:"
msgstr "分享为："
